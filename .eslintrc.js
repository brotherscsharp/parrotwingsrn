module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'sonarjs', '@typescript-eslint'],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/no-shadow': ['error'],
        'sonarjs/cognitive-complexity': 'error',
        'sonarjs/no-identical-expressions': 'error',
        '@typescript-eslint/no-explicit-any': 'error',
        'no-shadow': 'off',
        'no-undef': 'off',
      },
    },
  ],
};
