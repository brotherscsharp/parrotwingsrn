# Parrot Wings React Natice

Test application on React Natice


### Run instructions for Android:
    1. Have an Android emulator running (quickest way to get started), or a device connected.
    2. cd "/parrotwings" && npx react-native run-android

### Run instructions for iOS:
    1. cd "/parrotwings" && npx react-native run-ios
    - or -
    1. Open parrotwings/ios/parrotwings.xcworkspace in Xcode or run "xed -b ios"
    2. Hit the Run button

### Run instructions for macOS:
    1. See https://aka.ms/ReactNativeGuideMacOS for the latest up-to-date instructions.