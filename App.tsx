import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import HomeScreen from './src/screens/HomeScreen';
import SendFundsScreen from './src/screens/SendFundsScreen';
import {persistStore} from 'redux-persist';
import {PersistGate} from 'redux-persist/integration/react';

import LoginScreen from './src/screens/LoginScreen';
import store from './src/store';
import {NativeBaseProvider, Box} from 'native-base';

const Stack = createStackNavigator();

export const persistor = persistStore(store);

import {SafeAreaView, Platform, StatusBar} from 'react-native';

const App = () => {
  const [initialNavigationState] = React.useState();
  const containerRef = React.useRef();

  return (
    <NativeBaseProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaView>
            <Box bg="#000" height={'100%'}>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <NavigationContainer
                ref={containerRef}
                initialState={initialNavigationState}
              >
                <Stack.Navigator>
                  <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{title: 'Parrot wings'}}
                  />

                  <Stack.Screen
                    name="SendFundsScreen"
                    component={SendFundsScreen}
                    options={{title: 'Отправка средств'}}
                  />

                  <Stack.Screen
                    name="LoginScreen"
                    component={LoginScreen}
                    options={{title: 'Авторизация'}}
                  />
                </Stack.Navigator>
              </NavigationContainer>
            </Box>
          </SafeAreaView>
        </PersistGate>
      </Provider>
    </NativeBaseProvider>
  );
};
export default App;
