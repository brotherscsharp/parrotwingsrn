import * as React from 'react';
import {TransactionItem} from '../components/TransactionItem';
import {StyleSheet, ScrollView} from 'react-native';
import {List} from 'native-base';
import {ITransaction} from '../protocols/ITransaction';

interface TransactionProps {
  readonly transactions: readonly ITransaction[];
  readonly onTransactionSelected: (item: ITransaction) => void;
}

export const Transactions = ({
  transactions,
  onTransactionSelected,
}: TransactionProps) => {
  const getList = () => {
    return (
      <List>
        {transactions &&
          transactions.map((item) => (
            <TransactionItem
              transaction={item}
              key={item.id.toString()}
              onTap={() => onTransactionSelected(item)}
            />
          ))}
      </List>
    );
  };

  return <ScrollView style={styles.scrollContainer}>{getList()}</ScrollView>;
};

const styles = StyleSheet.create({
  scrollContainer: {
    overflow: 'hidden',
  },
  scrollContainer1: {
    overflow: 'hidden',
  },
});
