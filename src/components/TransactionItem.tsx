import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Strings from '../constants/Strings';
import Moment from 'moment';
import {ITransaction} from '../protocols/ITransaction';

interface TransactionItemProps {
  readonly transaction: ITransaction;
  readonly onTap: (id: number) => void;
}
export const TransactionItem = ({transaction, onTap}: TransactionItemProps) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onTap.bind(null, transaction.id)}
    >
      <View style={styles.transaction}>
        <View style={styles.iconContainer}>
          <Image
            style={styles.icon}
            source={
              transaction.amount < 0
                ? require('../../assets/images/out.png')
                : require('../../assets/images/in.png')
            }
          />
        </View>
        <View style={styles.description}>
          <Text style={styles.boldText}>{transaction.username}</Text>
          <Text style={styles.smallText}>
            {Moment(new Date(transaction.date)).format('D.MM.YYYY H:mm')}
          </Text>
        </View>
        <View style={styles.amounts}>
          <Text style={styles.boldText}>
            {transaction.amount} {Strings.currencySign}
          </Text>
          <Text style={styles.smallText}>
            {transaction.balance} {Strings.currencySign}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    width: 30,
  },
  transaction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 2,
    padding: 10,
    borderColor: '#95D4E9',
    borderRadius: 5,
    marginBottom: 10,
    marginLeft: 0,
  },
  description: {
    width: '70%',
  },
  amounts: {
    width: '20%',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  icon: {
    width: 20,
    height: 20,
  },
  smallText: {
    fontSize: 12,
    marginTop: 10,
  },
  boldText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});
