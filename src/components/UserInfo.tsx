import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import Strings from '../constants/Strings';
import {Button, Text} from 'native-base';
import {useDispatch} from 'react-redux';
import {IUserInfo} from '../protocols/IUserInfo';
import {logout} from '../reducers/AuthReducer';

interface UserInfoProps {
  readonly userInfo?: IUserInfo;
}

export const UserInfo = ({userInfo}: UserInfoProps) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.userInfoBlock}>
      {userInfo && (
        <View style={styles.description}>
          <Text style={styles.boldText}>{userInfo.name}</Text>
          <Text>
            Баланс: {userInfo.balance} {Strings.currencySign}
          </Text>
        </View>
      )}

      <View style={styles.amounts}>
        <Button style={styles.button} onPress={() => dispatch(logout())}>
          <Text>Выйти</Text>
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  userInfoBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  description: {
    width: '60%',
  },
  amounts: {
    width: '39%',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  button: {
    width: 150,
    right: 0,
    position: 'absolute',
    top: -23,
  },
  boldText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});
