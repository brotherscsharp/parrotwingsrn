import * as React from 'react';
import {useState, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {PWImage, LogoSize} from './PWImage';
import {useSelector} from 'react-redux';
import {Button, Box, Input, Text, View, Toast, IToastProps} from 'native-base';

import {goToRegistration} from '../reducers/ParrotWingsReducer';
import {useDispatch} from 'react-redux';
import {isStringEmpty, isValidEmail} from '../common';
import Strings from '../constants/Strings';
import {RootState} from '../store';
import {loginAction} from '../reducers/AuthReducer';

export const SignIn = () => {
  const dispatch = useDispatch();

  const error: string = useSelector((state: RootState) => state.auth.error);

  //const [userName, setUserName] = useState('servin@sys.im');
  //const [password, setPassword] = useState('1234qwer');
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (error && error.length > 0) {
      showToastMessage(Strings.Errors[error], Strings.ToastType.danger);
    }
  }, [error]);

  const singIn = () => {
    if (isValid()) {
      dispatch(loginAction({email: userName, password}));
    }
  };

  const isValid = (): boolean => {
    // validate user name
    if (isStringEmpty(userName)) {
      showToastMessage(
        Strings.Validation.userNameCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }
    if (!isValidEmail(userName)) {
      showToastMessage(
        Strings.Validation.emailIsNotValid,
        Strings.ToastType.warning,
      );
      return false;
    }

    // validate password
    if (isStringEmpty(password)) {
      showToastMessage(
        Strings.Validation.passwordCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    return true;
  };

  const showToastMessage = (message: string, type: string) => {
    const prp: IToastProps = {
      title: message,
      placement: 'top',
      status: type,
    };
    Toast.show(prp);
  };

  return (
    <Box>
      <ScrollView>
        <Box>
          <PWImage size={LogoSize.big} />
        </Box>

        <View>
          <Input
            value={userName}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Введите e-mail"
            onChangeText={setUserName}
          />
        </View>

        <View>
          <Input
            value={password}
            secureTextEntry={true}
            placeholder="Введите пароль"
            onChangeText={setPassword}
          />
        </View>

        <Button style={styles.button} onPress={singIn}>
          <Text>Войти</Text>
        </Button>

        <Button
          style={styles.button}
          onPress={() => dispatch(goToRegistration(false))}
        >
          <Text>или зарегистрируйся</Text>
        </Button>
      </ScrollView>
    </Box>
  );
};

const styles = {
  button: {
    marginTop: 15,
  },
};
