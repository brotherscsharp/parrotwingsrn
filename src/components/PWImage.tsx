import * as React from 'react';
import {Image, StyleSheet} from 'react-native';
import {Box} from 'native-base';

interface PWImageProps {
  size: LogoSize;
}

export const PWImage = ({size}: PWImageProps) => {
  const getLogoSize = (imageSize: LogoSize) => {
    switch (imageSize) {
      case LogoSize.big:
        return styles.bigLogo;
      case LogoSize.medium:
        return styles.mediumLogo;
      case LogoSize.small:
        return styles.smallLogo;
      default:
        return styles.smallLogo;
    }
  };

  return (
    <Box style={styles.logoContainer}>
      <Image
        style={getLogoSize(size)}
        source={require('../../assets/images/pw.jpg')}
      />
    </Box>
  );
};

export enum LogoSize {
  small,
  medium,
  big,
}

const styles = StyleSheet.create({
  logoContainer: {
    alignItems: 'center',
    height: 150,
  },
  smallLogo: {
    width: 50,
    height: 50,
  },
  mediumLogo: {
    width: 100,
    height: 100,
  },
  bigLogo: {
    width: 150,
    height: 150,
  },
});
