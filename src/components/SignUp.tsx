import * as React from 'react';
import {useState, useEffect} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {PWImage, LogoSize} from './PWImage';
import {Input, Box, Button, Text, Toast, IToastProps} from 'native-base';
import {goToRegistration} from '../reducers/ParrotWingsReducer';
import {useDispatch} from 'react-redux';
import {isStringEmpty, isValidEmail} from '../common';
import Strings from '../constants/Strings';
import {useSelector} from 'react-redux';
import {RootState} from '../store';
import {registerAction} from '../reducers/AuthReducer';

export const SignUp = () => {
  const dispatch = useDispatch();

  //const [userName, setUserName] = useState<string>('servin');
  //const [email, setEmail] = useState<string>('servin@sys.im');
  //const [password, setPassword] = useState<string>('1234qwer');
  //const [passwordRepeate, setPasswordRepeate] = useState<string>('1234qwer');
  
  const [userName, setUserName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [passwordRepeate, setPasswordRepeate] = useState<string>('');

  const error: string = useSelector((state: RootState) => state.auth.error);

  const singUp = () => {
    if (isValid()) {
      dispatch(registerAction({email, password, username: userName}));
    }
  };

  useEffect(() => {
    if (error && error.length > 0) {
      showToastMessage(Strings.Errors[error], Strings.ToastType.danger);
    }
  }, [error]);

  const showToastMessage = (message: string, type: string) => {
    const prp: IToastProps = {
      title: message,
      placement: 'top',
      status: type,
    };
    Toast.show(prp);
  };

  const isValid = (): boolean => {
    // validate user name
    if (isStringEmpty(userName)) {
      showToastMessage(
        Strings.Validation.userNameCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    // validate email
    if (isStringEmpty(email)) {
      showToastMessage(
        Strings.Validation.emailCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }
    if (!isValidEmail(email)) {
      showToastMessage(
        Strings.Validation.emailIsNotValid,
        Strings.ToastType.warning,
      );
      return false;
    }

    // validate password
    if (isStringEmpty(password)) {
      showToastMessage(
        Strings.Validation.passwordCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    if (isStringEmpty(passwordRepeate)) {
      showToastMessage(
        Strings.Validation.passwordCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    if (password !== passwordRepeate) {
      showToastMessage(
        Strings.Validation.passwordsAreNotMatch,
        Strings.ToastType.warning,
      );
      return false;
    }

    return true;
  };

  return (
    <Box>
      <ScrollView>
        <Box>
          <PWImage size={LogoSize.big} />
        </Box>

        <Box>
          <Input
            placeholder="Введите логин"
            value={userName}
            onChangeText={setUserName}
          />
        </Box>

        <Box>
          <Input
            placeholder="Введите e-mail"
            value={email}
            onChangeText={setEmail}
          />
        </Box>

        <Box>
          <Input
            value={password}
            secureTextEntry={true}
            placeholder="Введите пароль"
            onChangeText={setPassword}
          />
        </Box>

        <Box>
          <Input
            value={passwordRepeate}
            secureTextEntry={true}
            placeholder="Повторите пароль"
            onChangeText={setPasswordRepeate}
          />
        </Box>

        <Button style={styles.button} onPress={singUp}>
          <Text>Зарегистрироваться</Text>
        </Button>

        <Button
          style={styles.button}
          onPress={() => dispatch(goToRegistration(true))}
        >
          <Text>или войди</Text>
        </Button>
      </ScrollView>
    </Box>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 15,
  },
});
