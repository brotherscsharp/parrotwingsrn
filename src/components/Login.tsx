import * as React from 'react';
import {Button, Text, Box} from 'native-base';
import {StyleSheet, View} from 'react-native';
import {PWImage, LogoSize} from './PWImage';

interface LoginProps {
  readonly onLoginButtonClicked: () => void;
}

export const Login = ({onLoginButtonClicked}: LoginProps) => {
  return (
    <Box style={styles.container}>
      <View>
        <View style={styles.logoContainer}>
          <PWImage size={LogoSize.big} />
        </View>

        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>
            Вам необходимо авторизоваться для использования приложения Parrot
            wings
          </Text>
        </View>

        <Button style={styles.loginButton} onPress={onLoginButtonClicked}>
          <Text>Войти</Text>
        </Button>
      </View>
    </Box>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  logoContainer: {
    alignItems: 'center',
    height: 150,
    flexDirection: 'column',
    margin: 10,
    borderWidth: 0,
    borderColor: 'transparent',
  },
  descriptionContainer: {
    flexDirection: 'column',
    margin: 10,
    borderWidth: 0,
    borderColor: 'transparent',
  },
  description: {
    textAlign: 'center',
  },
  loginButton: {
    marginBottom: 10,
  },
});
