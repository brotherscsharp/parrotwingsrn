export default {
  currencySign: 'PW',

  ToastType: {
    warning: 'warning',
    danger: 'error',
    success: 'success',
  },

  ToastDuration: {
    quick: 150,
    normal: 200,
    long: 350,
  },

  Messages: {
    sendFundsSuccess: 'Сумма успешно отправлена',
    registrationSuccess: 'Вы успешно зарегистрированы',
  },

  Validation: {
    amountCantBeEmpty: 'Сумма не может быть пустой',
    sameUserName: 'Нельзя отправлять PW самому себе',
    userNameCantBeEmpty: 'Имя пользователя не может быть пустым',
    amountShouldBeGreatThenZero: 'Сумма должна быть больше 0',
    emailCantBeEmpty: 'E-mail не может быть пустым',
    emailIsNotValid: 'E-mail не валидный',
    passwordCantBeEmpty: 'Пароль не может быть пустым',
    passwordsAreNotMatch: 'Пароли не совпадают',
  },

  Errors: {
    'Invalid username.': 'Некорректное имя пользователя',
    'UnauthorizedError: No Authorization header was found':
      'Необходимо авторизоваться',
    'Balance exceeded. Transaction is impossible':
      'Недостаточно средств на счету',
    'A user with that email exists': 'Пользователь таким email уже существует',
    'You must send username and password':
      'Необходимо указать имя пользователя и пароль',
    'You must send email and password.':
      'Необходимо указать имя пользователя и пароль',
    'Invalid email or password.': 'Неверный логин или пароль',
  },

  endpoints: {
    serverBaseUrl: 'http://193.124.114.46:3001',
    register: '/users',
    login: '/sessions/create',
    userTransactions: '/api/protected/transactions',
    sendFunds: '/api/protected/transactions',
    currentUserInfo: '/api/protected/user-info',
    filterUsers: '/api/protected/users/list',
  },
};
