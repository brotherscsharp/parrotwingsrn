import {
  createSlice,
  PayloadAction,
  createAsyncThunk,
  unwrapResult,
} from '@reduxjs/toolkit';
import {IHomeScreenProps} from '../protocols/IHomeScreenProps';
import {AuthService} from '../networking/AuthService';
import {RemoteService} from '../networking/RemoteService';
import {ITransaction} from '../protocols/ITransaction';
import {WritableDraft} from 'immer/dist/internal';
import {IUserInfo} from '../protocols/IUserInfo';

export const INITIAL_STATE: IHomeScreenProps = {
  userInfo: {
    name: '',
    balance: 0,
    id: 0,
    email: '',
    error: '',
  },
  transactions: [],
  isLoginForm: true,
};

interface ITransObject {
  readonly trans_token: readonly ITransaction[];
  readonly error: string;
}

export const getUserInfoThunk = createAsyncThunk(
  'parrotWings/getUserInfoThunk',
  // @ts-ignore
  async (payload: string, {}) => {
    try {
      const res = await RemoteService.getUserInfo(payload);
      return res.data.user_info_token as IUserInfo;
    } catch (error) {
      return {error: error.response};
    }
  },
);

export const getUserTransactionsThunk = createAsyncThunk(
  'parrotWings/getUserTransactionsThunk',
  // @ts-ignore
  async (payload: string, {}) => {
    try {
      const res = await RemoteService.getUserTransactions(payload);
      return {trans_token: res.data.trans_token as readonly ITransaction[]};
    } catch (error) {
      return {error: error.response};
    }
  },
);

const parrotWingsSlice = createSlice({
  name: 'parrotWings',
  initialState: INITIAL_STATE,
  reducers: {
    goToRegistration: (
      state: WritableDraft<IHomeScreenProps>,
      action: PayloadAction<boolean>,
    ) => {
      state.isLoginForm = action.payload;
    },
    isUserLoggedIn: (state: WritableDraft<IHomeScreenProps>) => {
      state.isAuthorized = AuthService.isUserLoggedIn(state.token);
    },
    clearUserData: (state: WritableDraft<IHomeScreenProps>) => {
      state.userInfo.name = undefined;
      state.userInfo.balance = undefined;
      state.transactions = undefined;
    },
  },
  extraReducers: {
    // @ts-ignore
    [getUserInfoThunk.fulfilled]: (
      state: WritableDraft<IHomeScreenProps>,
      action: PayloadAction<IUserInfo>,
    ) => {
      if (getUserInfoThunk.fulfilled.match(action)) {
        const user = unwrapResult(action);

        if (user) {
          if (user.error) {
            state.error = user.error.data;
            state.token = undefined;
            state.isAuthorized = false;
            console.log(`Update failed: ${user.error.data}`);
          } else {
            const userInfo = user;
            state.userInfo.id = userInfo.id;
            state.userInfo.name = userInfo.name;
            state.userInfo.email = userInfo.email;
            state.userInfo.balance = userInfo.balance;
          }
        }
      } else {
        console.log(`Update failed: ${action.payload.error}`);
      }
    },
    // @ts-ignore
    [getUserTransactionsThunk.fulfilled]: (
      state: WritableDraft<IHomeScreenProps>,
      action: PayloadAction<ITransObject>,
    ) => {
      if (getUserTransactionsThunk.fulfilled.match(action)) {
        const user = unwrapResult(action);
        console.log('<<<<<<< 4 >>>>>>', user);
        if (user.trans_token) {
          state.transactions = user.trans_token.sort((one, two) =>
            one > two ? 1 : -1,
          );
        }
      } else {
        console.log(`Update failed: ${action.payload.error}`);
      }
    },
  },
});

export const {goToRegistration, clearUserData, isUserLoggedIn} =
  parrotWingsSlice.actions;

export default parrotWingsSlice.reducer;
