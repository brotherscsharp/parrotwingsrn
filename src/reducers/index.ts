import {combineReducers} from 'redux';
import ParrotWingsReducer from './ParrotWingsReducer';
import SendFundsReducer from './SendFundsReducer';
import authReducer from './AuthReducer';

export default combineReducers({
  pw: ParrotWingsReducer,
  funds: SendFundsReducer,
  auth: authReducer,
});
