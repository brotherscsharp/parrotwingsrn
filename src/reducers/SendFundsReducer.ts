import {createSlice, createAsyncThunk, unwrapResult} from '@reduxjs/toolkit';
import {RemoteService} from '../networking/RemoteService';
import {ITransaction} from '../protocols/ITransaction';
import {IUser} from '../protocols/IUser';
import {ISendFundsProps} from '../protocols/ISendFundsProps';
import {RootState} from '../store';
import {getUserInfoThunk, getUserTransactionsThunk} from './ParrotWingsReducer';

export const INITIAL_STATE: ISendFundsProps = {
  filteredUsers: [],
  transaction: undefined,
};
export const getUserByNameThunk = createAsyncThunk(
  'sendFundsSlice/getUserByNameThunk',
  async (payload: {readonly name: string}, {getState}) => {
    try {
      const state = getState() as RootState;
      const res = await RemoteService.getUserByName(
        payload.name,
        state.auth.token,
      );
      return {users: res.data as readonly IUser[]};
    } catch (error) {
      return {error: error.response};
    }
  },
);

export const sendFundsThunk = createAsyncThunk(
  'sendFundsSlice/sendFundsThunk',
  async (
    payload: {readonly name: string; readonly amount: number},
    {getState, dispatch},
  ) => {
    try {
      const state = getState() as RootState;
      const res = await RemoteService.sendFunds(
        payload.name,
        payload.amount,
        state.auth.token,
      );

      dispatch(getUserInfoThunk(state.auth.token));
      dispatch(getUserTransactionsThunk(state.auth.token));

      return {users: res.data.trans_token as ITransaction};
    } catch (error) {
      return {error: error.response};
    }
  },
);

const sendFundsSlice = createSlice({
  name: 'sendFundsSlice',
  initialState: INITIAL_STATE,
  reducers: {},
  extraReducers: {
    // @ts-ignore
    [getUserByNameThunk.fulfilled]: (
      state: RootState,
      action: UnwrappableAction,
    ) => {
      if (getUserByNameThunk.fulfilled.match(action)) {
        const user = unwrapResult(action);
        if (user.users) {
          state.filteredUsers = user.users;
        }
      } else {
        console.log(`Update failed: ${action.payload.error}`);
      }
    },
    // @ts-ignore
    [sendFundsThunk.fulfilled]: (
      state: RootState,
      action: UnwrappableAction,
    ) => {
      const data = unwrapResult(action);
      if (data) {
        if (data.error) {
          state.error = data.error.data;
          console.log(`Update failed: ${data.payload.error}`);
        }
      }
    },
  },
});

export default sendFundsSlice.reducer;
