import {
  createSlice,
  createAsyncThunk,
  unwrapResult,
  AnyAction,
} from '@reduxjs/toolkit';
import {AuthService} from '../networking/AuthService';
import {WritableDraft} from 'immer/dist/internal';
import {getUserInfoThunk, getUserTransactionsThunk} from './ParrotWingsReducer';

export interface IAuthState {
  readonly token: string | undefined;
  readonly isAuthorized: boolean;
  readonly error: string;
}

export interface IAuthData {
  readonly token?: string;
  readonly error?: unknown;
}

export const INITIAL_STATE: IAuthState = {
  token: '',
  isAuthorized: false,
  error: '',
};

export const loginAction = createAsyncThunk(
  'authReducer/loginAction',
  // @ts-ignore
  async (
    payload: {
      readonly email: string;
      readonly password: string;
    },
    {dispatch},
  ) => {
    try {
      const {email, password} = payload;
      const res = await AuthService.login(email, password);
      dispatch(getUserInfoThunk(res.data.id_token));
      dispatch(getUserTransactionsThunk(res.data.id_token));
      return {token: res.data.id_token as string};
    } catch (error) {
      //rejectWithValue(error);
      return {error: error.response};
    }
  },
);

export const registerAction = createAsyncThunk(
  'authReducer/registerAction',
  // @ts-ignore
  async (
    payload: {
      readonly email: string;
      readonly password: string;
      readonly username: string;
    },
    {dispatch},
  ) => {
    try {
      const {email, password, username} = payload;
      const res = await AuthService.register(email, password, username);

      dispatch(getUserInfoThunk(res.data.id_token));
      dispatch(getUserTransactionsThunk(res.data.id_token));
      return {token: res.data.id_token as string};
    } catch (error) {
      return {error: error.response};
    }
  },
);

const authReducerSlice = createSlice({
  name: 'authReducer',
  initialState: INITIAL_STATE,
  reducers: {
    errorShown: (state: WritableDraft<IAuthState>) => {
      state.error = '';
    },
    logout: (state: WritableDraft<IAuthState>) => {
      state.token = undefined;
      state.isAuthorized = false;
      state.error = '';
    },
  },
  extraReducers: {
    // @ts-ignore
    [loginAction.fulfilled]: (
      state: WritableDraft<IAuthState>,
      action: AnyAction,
    ) => {
      if (loginAction.fulfilled.match(action)) {
        const authData: IAuthData = unwrapResult(action);
        if (authData) {
          if (authData.error) {
            state.error = authData.error.data;
            state.token = undefined;
            state.isAuthorized = false;
            console.log(`Update failed: ${authData.error.data}`);
          } else {
            state.token = authData.token;
            state.isAuthorized = true;
            state.error = '';
          }
        }
      }
    },
    // @ts-ignore
    [registerAction.fulfilled]: (
      state: WritableDraft<IAuthState>,
      action: AnyAction,
    ) => {
      if (registerAction.fulfilled.match(action)) {
        const authData = unwrapResult(action);
        if (authData) {
          if (authData.error) {
            state.error = authData.error.data;
            state.token = undefined;
            state.isAuthorized = false;
            console.log(`Update failed: ${authData.error.data}`);
          } else {
            state.token = authData.token;
            state.isAuthorized = true;
            state.error = '';
          }
        }
      }
    },
  },
});

export const {errorShown, logout} = authReducerSlice.actions;

export default authReducerSlice.reducer;
