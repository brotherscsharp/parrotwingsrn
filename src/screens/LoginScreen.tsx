import React from 'react';
import {Box} from 'native-base';
import {connect} from 'react-redux';
import {IHomeScreenProps} from '../protocols/IHomeScreenProps';
import {SignIn} from '../components/SignIn';
import {SignUp} from '../components/SignUp';
import {isUserLoggedIn} from '../reducers/ParrotWingsReducer';
import {StyleSheet} from 'react-native';

class LoginScreen extends React.Component<
  IHomeScreenProps & {readonly navigation}
> {
  render() {
    const {isLoginForm, isAuthorized, navigation} = this.props;

    if (isAuthorized) {
      navigation.navigate('Home');
    }

    return (
      <Box style={styles.container}>
        {isLoginForm ? <SignIn /> : <SignUp />}
      </Box>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  sendFundsButton: {
    marginBottom: 10,
  },
});

const mapStateToProps = (state) => {
  return {
    isAuthorized: state.auth.isAuthorized,
    isLoginForm: state.pw.isLoginForm,
  };
};

const mapDispatchToProps = (dispatch) => ({
  cheskIsUserLoggedIn: () => dispatch(isUserLoggedIn(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
