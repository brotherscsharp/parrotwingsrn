import * as React from 'react';
import {Box, Input, Button, Toast, IToastProps} from 'native-base';
import {PWImage, LogoSize} from '../components/PWImage';
import {ISendFundsProps} from '../protocols/ISendFundsProps';
import {ISendFundsState} from '../protocols/ISendFundsState';
import {connect} from 'react-redux';
import {getUserByNameThunk, sendFundsThunk} from '../reducers/SendFundsReducer';
import {Text, StyleSheet} from 'react-native';
import {isStringEmpty, isCorrectAmount} from '../common';
import {
  getUserTransactionsThunk,
  getUserInfoThunk,
} from '../reducers/ParrotWingsReducer';
import Strings from '../constants/Strings';
import {UserInfo} from '../components/UserInfo';
import {ITransaction} from '../protocols/ITransaction';
import {AppDispatch, RootState} from '../store';

import {
  AutocompleteDropdown,
  TAutocompleteDropdownItem,
} from 'react-native-autocomplete-dropdown';

class SendFundsScreen extends React.Component<
  ISendFundsProps,
  ISendFundsState
> {
  constructor(props) {
    super(props);

    const transaction: ITransaction = props.route.params;

    this.state = {
      userQuery: transaction ? transaction.username : '',
      amount: transaction ? Math.abs(transaction.amount).toString() : '0',
      autocompleteListIsOpen: false,
      users: [],
    };
  }

  private readonly isValid = (): boolean => {
    const {userInfo} = this.props;
    // validate user name
    if (isStringEmpty(this.state.userQuery)) {
      this.showToastMessage(
        Strings.Validation.userNameCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    if (userInfo && this.state.userQuery === userInfo.name) {
      this.showToastMessage(
        Strings.Validation.sameUserName,
        Strings.ToastType.warning,
      );
      return false;
    }

    // validate amount
    if (isStringEmpty(this.state.amount)) {
      this.showToastMessage(
        Strings.Validation.amountCantBeEmpty,
        Strings.ToastType.warning,
      );
      return false;
    }

    if (!isCorrectAmount(this.state.amount)) {
      this.showToastMessage(
        Strings.Validation.amountShouldBeGreatThenZero,
        Strings.ToastType.warning,
      );
      return false;
    }

    return true;
  };

  private readonly showToastMessage = (message: string, type: string) => {
    const prp: IToastProps = {
      title: message,
      placement: 'top',
      status: type,
    };
    Toast.show(prp);
  };

  private readonly sendFunds = () => {
    const {userQuery, amount} = this.state;
    const {sendFunds, getUserTransactions, getUserInfo} = this.props;

    if (this.isValid()) {
      // send money to another user
      sendFunds &&
        sendFunds(userQuery, Number(amount)).then((res) => {
          // success
          if (!res?.payload?.error) {
            this.showToastMessage(
              Strings.Messages.sendFundsSuccess,
              Strings.ToastType.success,
            );
            this.clearForm();
            getUserInfo && getUserInfo();
            getUserTransactions && getUserTransactions();
          } else {
            // some error happened
            // here I have to parse string because we have different errors on same error code
            // eg. 401 UnauthorizedError and Invalid user
            this.showToastMessage(
              Strings.Errors[res.payload.error.data],
              Strings.ToastType.danger,
            );
          }
        });
    }
  };

  clearForm() {
    this.setState({
      userQuery: '',
      amount: '',
    });
  }

  private readonly getUser = (name: string) => {
    const {getUserByName} = this.props;
    this.setState({
      userQuery: name,
    });

    if (name && name.length > 0) {
      getUserByName &&
        getUserByName(name).then(() => {
          this.setState({
            users: this.props.filteredUsers,
          });
        });
    }
  };

  private readonly setQuery = (name?: string) => {
    if (name) {
      this.setState({userQuery: name, users: []});
    }
  };

  render() {
    const {users, amount} = this.state;
    const {userInfo} = this.props;

    const usedDataset: TAutocompleteDropdownItem[] = users.map((user) => ({
      id: user.id,
      title: user.name,
    }));

    return (
      <Box style={styles.container}>
        <UserInfo userInfo={userInfo} />
        <Box>
          <PWImage size={LogoSize.big} />
        </Box>

        <AutocompleteDropdown
          clearOnFocus={false}
          closeOnBlur={true}
          closeOnSubmit={false}
          onChangeText={(text: string) => this.getUser(text)}
          onSelectItem={(item: TAutocompleteDropdownItem) => {
            this.setQuery(item.title as string);
          }}
          dataSet={usedDataset}
        />

        <Box>
          <Input
            value={amount}
            placeholder="Введите сумму"
            keyboardType="number-pad"
            onChangeText={(amountVal: string) =>
              this.setState({amount: amountVal})
            }
          />
        </Box>

        <Button style={styles.button} onPress={() => this.sendFunds()}>
          <Text>Отправить</Text>
        </Button>
      </Box>
    );
  }
}

const styles = StyleSheet.create({
  listStyle: {
    height: 100,
    position: 'relative',
    zIndex: 999,
    padding: 0,
  },
  inputContainerStyle: {
    borderWidth: 0,
  },

  autocompleteContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    width: '100%',
  },
  itemSubText: {
    fontSize: 10,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
    marginLeft: 10,
  },

  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  button: {
    marginTop: 15,
  },
  autocompleteContainer1: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
});

const mapStateToProps = (state: RootState) => {
  return {
    userInfo: state.pw.userInfo,
    filteredUsers: state.funds.filteredUsers,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  getUserByName: (name: string) => dispatch(getUserByNameThunk({name})),
  sendFunds: (name: string, amount: number) =>
    dispatch(sendFundsThunk({name, amount})),
  getUserTransactions: (token: string) =>
    dispatch(getUserTransactionsThunk(token)),
  getUserInfo: (token: string) => dispatch(getUserInfoThunk(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SendFundsScreen);
