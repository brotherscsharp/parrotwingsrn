import React from 'react';
import {UserInfo} from '../components/UserInfo';
import {Box, Button, Text} from 'native-base';
import {connect} from 'react-redux';
import {IHomeScreenProps} from '../protocols/IHomeScreenProps';
import {isUserLoggedIn} from '../reducers/ParrotWingsReducer';
import {Transactions} from '../components/Transactions';
import {StyleSheet} from 'react-native';
import {ITransaction} from '../protocols/ITransaction';
import {Login} from '../components/Login';

class HomeScreen extends React.Component<
  IHomeScreenProps & {readonly navigation}
> {
  readonly goToSendFunds = (transaction: ITransaction = undefined) => {
    const {navigation} = this.props;
    navigation.navigate('SendFundsScreen', transaction);
  };

  readonly goToLoginScreen = () => {
    const {navigation} = this.props;
    navigation.navigate('LoginScreen');
  };

  render() {
    const {userInfo, isAuthorized, transactions} = this.props;

    return (
      <Box style={styles.mainContainer}>
        {isAuthorized ? (
          <Box>
            <UserInfo userInfo={userInfo} />
            <Button
              style={styles.sendFundsButton}
              onPress={() => this.goToSendFunds()}
            >
              <Text>Отправить PW</Text>
            </Button>
            <Transactions
              transactions={transactions}
              onTransactionSelected={(transaction) =>
                this.goToSendFunds(transaction)
              }
            />
          </Box>
        ) : (
          <Login onLoginButtonClicked={() => this.goToLoginScreen()} />
        )}
      </Box>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  sendFundsButton: {
    marginBottom: 10,
  },
});

const mapStateToProps = (state) => {
  return {
    userInfo: state.pw.userInfo,
    isLoginForm: state.pw.isLoginForm,
    isAuthorized: state.auth.isAuthorized,
    transactions: state.pw.transactions,
  };
};

const mapDispatchToProps = (dispatch) => ({
  cheskIsUserLoggedIn: () => dispatch(isUserLoggedIn(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
