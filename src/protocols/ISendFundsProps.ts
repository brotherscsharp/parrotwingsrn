import {IUser} from './IUser';
import {ICommonProps} from './IHomeScreenProps';
import {ITransaction} from './ITransaction';
import {IUserInfo} from './IUserInfo';
import {RouteProp} from '@react-navigation/core';

export interface AppListenersContainerProps {
  readonly route?: RouteProp<any, never>;
}

export interface ISendFundsProps
  extends ICommonProps,
    AppListenersContainerProps {
  readonly transaction: ITransaction | undefined;
  readonly filteredUsers: readonly IUser[];
  readonly getUserByName?: (name: string) => Promise<IUserInfo>;
  readonly sendFunds?: (name: string, amount: number) => Promise<unknown>;
}
