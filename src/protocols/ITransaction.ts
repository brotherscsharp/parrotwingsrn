export interface ITransaction {
  readonly id: number;
  readonly date: Date;
  readonly username: string;
  readonly balance: number;
  readonly amount: number;
}
