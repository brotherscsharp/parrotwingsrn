export interface IUserInfo {
  readonly id: number;
  readonly name: string;
  readonly email: string;
  readonly balance: number;
  readonly error: string;
}
