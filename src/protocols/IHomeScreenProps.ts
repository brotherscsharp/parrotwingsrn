import {IUserInfo} from './IUserInfo';
import {ITransaction} from './ITransaction';

export interface IHomeScreenProps extends ICommonProps {
  readonly token?: string;
  readonly isLoginForm: boolean;
  readonly isAuthorized?: boolean;
  readonly transactions: readonly ITransaction[];
  readonly cheskIsUserLoggedIn?: () => void;
}

export interface ICommonProps {
  readonly userInfo?: IUserInfo;
  readonly getUserInfo?: () => void;
  readonly getUserTransactions?: () => void;
}
