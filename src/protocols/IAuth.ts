export interface IAuth {
  readonly email: string;
  readonly password: string;
}
