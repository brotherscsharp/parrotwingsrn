import {IUser} from './IUser';

export interface ISendFundsState {
  readonly userQuery: string;
  readonly amount: string;
  readonly autocompleteListIsOpen: boolean;
  readonly users: readonly IUser[];
}
