export interface ILoginState {
  readonly userName: string;
  readonly password: string;
}
