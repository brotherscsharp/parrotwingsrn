export interface ILoginProps {
  readonly userName: string;
  readonly password: string;
  readonly switchToRegistration?: (x: boolean) => void;
  readonly loginUser?: (email: string, password: string) => void;
}
