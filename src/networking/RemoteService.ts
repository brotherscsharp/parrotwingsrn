import Strings from '../constants/Strings';
import axios from 'axios';

export class RemoteService {
  static readonly getUserInfo = async (token: string) => {
    const ax = RemoteService.getAxios(token);
    return await ax.get(Strings.endpoints.currentUserInfo);
  };

  static readonly getUserTransactions = async (token: string) => {
    const ax = RemoteService.getAxios(token);
    return await ax.get(Strings.endpoints.userTransactions);
  };

  static readonly getUserByName = async (name: string, token: string) => {
    const ax = RemoteService.getAxios(token);
    return await ax.post(Strings.endpoints.filterUsers, {filter: name});
  };

  static readonly sendFunds = async (
    name: string,
    amount: number,
    token: string,
  ) => {
    const ax = RemoteService.getAxios(token);
    return await ax.post(Strings.endpoints.sendFunds, {name, amount});
  };

  static getAxios(token: string) {
    if (token) {
      return axios.create({
        baseURL: Strings.endpoints.serverBaseUrl,
        timeout: 5000,
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      });
    } else {
      return axios.create({
        baseURL: Strings.endpoints.serverBaseUrl,
        timeout: 5000,
      });
    }
  }
}
