import Strings from '../constants/Strings';
import axios from 'axios';

export class AuthService {
  static readonly login = async (email: string, password: string) => {
    return await axios.post(
      `${Strings.endpoints.serverBaseUrl}${Strings.endpoints.login}`,
      {email, password},
    );
  };

  static readonly register = async (
    email: string,
    password: string,
    username: string,
  ) => {
    return await axios.post(
      `${Strings.endpoints.serverBaseUrl}${Strings.endpoints.register}`,
      {email, password, username},
    );
  };

  static readonly isUserLoggedIn = (token: string): boolean => {
    return token ? true : false;
  };
}
