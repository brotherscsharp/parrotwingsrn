export const sortByDate = (a: Date, b: Date) => (a > b ? -1 : a < b ? 1 : 0);

export const isStringEmpty = (value: string) =>
  !value || value.trim().length === 0;

export const isValidEmail = (value: string) => {
  const regexp = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
  return regexp.test(String(value).toLowerCase());
};

export const isCorrectAmount = (value: string) => {
  const amount = Number(value);
  return amount && !isNaN(amount) && amount > 0;
};
